set nocompatible
runtime macros/matchit.vim
    filetype indent plugin on
set is
set encoding=utf-8
call plug#begin()
let g:ackprg = 'ag --nogroup --nocolor --column'
" ========== autocomplete
Plug 'mattn/emmet-vim'
Plug 'ervandew/supertab'
Plug 'luochen1990/rainbow'
let g:rainbow_active = 0 "0
let mapleader = ","

" ========= snippets
Plug 'Yggdroot/indentLine'
Plug 'mhinz/vim-startify'
let g:indentLine_char = '∣'
let g:indentLine_fileTypeExclude = [ 'startify' ]

" set up ascii for my startify
let g:startify_custom_header = [
    \ '',
    \ '                        _         ___                  _',
    \ '                       | |       / __)                | |         _     _',
    \ '                       | |____ _| |__ ___  ____  _____| | _____ _| |_ _| |_ _____',
    \ '                       | |  _ (_   __) _ \|  _ \(____ | || ___ (_   _|_   _) ___ |',
    \ '                       | | | | || | | |_| | |_| / ___ | || ____| | |_  | |_| ____|',
    \ '                       |_|_| |_||_|  \___/|  __/\_____|\_)_____)  \__)  \__)_____)',
    \ '                                          |_|',
    \ '',
    \ ]
let g:ale_fix_on_save = 1

autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" ========== colorschemes
Plug 'flazz/vim-colorschemes'
Plug 'cakebaker/scss-syntax.vim'
Plug 'craigemery/vim-autotag'
Plug 'edkolev/tmuxline.vim'
Plug 'michaeljsmith/vim-indent-object'

" =========== git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'jparise/vim-graphql'
Plug 'vim-syntastic/syntastic'

" ==== enable matchit
" =========== markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'dyng/ctrlsf.vim' " for really nice search results like in sublime
Plug 'yonchu/accelerated-smooth-scroll'
Plug 'vim-scripts/IndexedSearch'
Plug 'itspriddle/vim-jquery'
Plug 'tpope/vim-haml'
Plug 'junegunn/goyo.vim'
Plug 'ecomba/vim-ruby-refactoring'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-bundler'
"========= syntax helpers
Plug 'rvesse/vim-sparql'
Plug 'hail2u/vim-css3-syntax'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-commentary'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'Chiel92/vim-autoformat'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
  let g:closetag_filenames = '*.html,*.xhtml,*.xml,*.vue,*.php,*.phtml,*.js,*.jsx,*.coffee,*.erb'


"======= eslint
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'
let g:syntastic_ruby_checkers = ['rubocop']

" ========= file tree
Plug  'scrooloose/nerdtree'

  let NERDTreeIgnore = [ '__pycache__',  '\.pyc$', '\.o$', '\.swp', '*\.swp', 'node_modules/' ]
  let NERDTreeShowHidden=1
  let NERDTreeQuitOnOpen=1
" ========= navigation
Plug 'christoomey/vim-tmux-navigator'
  " autostart nerd-tree
  autocmd StdinReadPre * let s:std_in=1
  " nerdtree toggle
  map <C-t> :NERDTreeToggle<CR>
Plug 'zhaocai/GoldenView.Vim'
  let g:goldenview__enable_default_mapping = 0
Plug 'benmills/vimux'
  " vimux binding
  map <Leader>vp :VimuxPromptCommand<CR>
  nmap <F8> :TagbarToggle<CR>

" ======= fuzzy find
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-eunuch'

" ======= Clojure
Plug 'tpope/vim-fireplace', { 'for' : 'clojure' }

" ======= extras
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
Plug 'majutsushi/tagbar'
Plug 'wincent/command-t'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mileszs/ack.vim'
if !exists('g:airline_symbols')
   let g:airline_symbols = {}
endif
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.crypt = '🔒'
" let g:airline_symbols.linenr = '☰'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.maxlinenr = ''
" let g:airline_symbols.maxlinenr = '㏑'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'ρ'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'
" let g:airline_symbols.spell = 'Ꞩ'
" let g:airline_symbols.notexists = 'Ɇ'
" let g:airline_symbols.whitespace = 'Ξ'

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'

call plug#end()            " required

" ============= extra settings

" tabs to 2 spaces
" set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set ruler
set hidden
:set guioptions-=m " remove menu bar
:set guioptions-=T " remove toolbar
:set guioptions-=r " remove right-hand scroll bar
:set guioptions-=L " remove left-hand scroll bar
":set lines=999 columns=999
set shortmess+=A " disable swap file warning

" hybrid line numbers
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
augroup END

" colorschemes
" Dark: monokai-chris, gruvbox
" Light: ChocolatePapaya
Plug 'morhetz/gruvbox'
colorscheme gruvbox
  let g:gruvbox_contrast_dark='default'
  let g:gruvbox_contrast_light='default'
" split below and right feels more natural
set splitbelow
set background=dark
" no wrapping
set nowrap

" allow backspace immediately after insert
set bs=2

" useful aliases
cnoreabbrev W w
cnoreabbrev Q q

" save undo in a file
set undofile
set undodir=~/.vim/undo
set undolevels=1000
set undoreload=10000


" tmux will only forward escape sequences to the terminal if surrounded by a
" DCS sequence
" "
" http://sourceforge.net/mailarchive/forum.php?thread_name=AANLkTinkbdoZ8eNR1X2UobLTeww1jFrvfJxTMfKSq-L%2B%40mail.gmail.com&forum_name=tmux-users
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

map ; :Files<CR>
